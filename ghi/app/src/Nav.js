import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-info">
      <div className="container-fluid nav">
        <NavLink className="navbar-brand fst-italic fw-bold" to="/">CarCar</NavLink>

        <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="http://localhost:8090/api/sales/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu">
                  <li><NavLink className="nav-link" aria-current="page" to="sales/customers-list/">List all customers</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/sales/sales-list/">List all sales</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/sales/salesperson-list/">List all salespeople</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/sales/create/">Create sale</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/sales/create-salesperson/">Create salesperson</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/sales/create-customer/">Create customer</NavLink></li>
                </ul>
        </li>

        <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="http://localhost:8080/api/service" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Services
                </a>
                <ul className="dropdown-menu">
                  <li><NavLink className="nav-link" aria-current="page" to="/service/list/">List service appointments</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/service/create/">Create service appointment</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/service/history/">Service History</NavLink></li>

                </ul>
        </li>

        <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="http://localhost:8080/api/technicians" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Technicians
                </a>
                <ul className="dropdown-menu">
                  <li><NavLink className="nav-link" aria-current="page" to="/technicians/create/">Create technician</NavLink></li>
                </ul>
        </li>

        <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="http://localhost:8100/api/inventory" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Inventory
                </a>
                <ul className="dropdown-menu">
                  <li><NavLink className="nav-link" aria-current="page" to="/manufacturers/list/">List manufacturers</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/models/list/">List models</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/automobiles/list/">List automobiles</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/manufacturers/create/">Create manufacturer</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/models/create/">Create model</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/automobiles/create/">Create automobile</NavLink></li>
                </ul>
        </li>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon">
          <ul className="dropdown-menu">
                  <li><NavLink className="nav-link" aria-current="page" to="/locations/new">List manufacturers</NavLink></li>
                  <li><NavLink className="nav-link" aria-current="page" to="/locations">List models</NavLink></li>
          </ul>
          </span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
