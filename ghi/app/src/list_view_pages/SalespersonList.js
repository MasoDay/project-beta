import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


    const SalespersonList = () => {
        const [ salespeople, setSalespeople ] = useState([])

        const getSalespersonData = async () => {
            const url = 'http://localhost:8090/api/salesperson/';
            const response = await fetch (url);

            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            } else {
                console.log("There was an error displaying salespeople.")
            }
        }


    useEffect(() => {
        getSalespersonData();
    }, []
    )

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salespeople</h1>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                salespeople?.map(salesperson => {
                                return(
                                    <tr key={ salesperson.id }>
                                        <td>{ salesperson.first_name }</td>
                                        <td>{ salesperson.last_name }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                    <Link to="/sales/create-salesperson/"><button className="btn btn-primary">Create new salesperson</button></Link>
                </div>
            </div>
        </div>
    );
}

export default SalespersonList;
