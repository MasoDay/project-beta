import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


const CustomerList = () => {
    const [ customers, setCustomers ] = useState([])

    const getCustomerData = async () => {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        } else {
            console.log("There was an error loading the customers list")
        }
    }

    useEffect(() => {
        getCustomerData();
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Customers</h1>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {customers.map(customer => {
                                    return(
                                        <tr key={ customer.id }>
                                            <td>{ customer.first_name }</td>
                                            <td>{ customer.last_name }</td>
                                        </tr>
                                );
                            })}
                        </tbody>
                    </table>
                    <Link to="/sales/create-customer/"><button className="btn btn-primary">Create new customer</button></Link>
                </div>
            </div>
        </div>
    );
}

export default CustomerList;
