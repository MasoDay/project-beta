import { useState, useEffect } from 'react';


function VehicleModelsList() {
    const [models, setModels] = useState([])
    const getData = async() => {
        const response = await fetch('http://localhost:8100/api/models/')
        const data = await response.json()
        setModels(data.models)
    }
    useEffect(()=> {
        getData();
    }, []
    )

    return (
        <div>
            <h1>Vehicle Models</h1>
            <table className="table table-dark table-hover table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} className="img-fluid" alt="Vehicle described above" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default VehicleModelsList;
