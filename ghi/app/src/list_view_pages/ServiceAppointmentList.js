import { useState, useEffect } from "react";


function ServiceAppointmentsList() {
    const [appts, setAppts] = useState([])

    const getData = async() => {
        const response = await fetch('http://localhost:8080/api/service/')
        const data = await response.json()
        setAppts(data.appts)
    }

    const handleDelete = async(id) => {
        const response = await fetch(`http://localhost:8080/api/service/${id}/`, {method:"DELETE"})
        const data = await response.json();
        getData();
        window.location.reload();
    }

    useEffect(()=> {
        getData();
    }, []
    )

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-dark table-hover table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Owner</th>
                        <th>Date</th>
                        <th>Assigned Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appts?.map(appt => {
                        return(
                            <tr key={appt.id}>
                                <td>{appt.vin}</td>
                                <td>{appt.owner}</td>
                                <td>{appt.date_time}</td>
                                <td>{appt.assigned_tech.id}</td>
                                <td>{appt.reason}</td>
                                <td>
                                <button>Finish</button>
                                </td>
                                <td>
                                <button onClick={()=> {handleDelete(appt.id)}}>Cancel</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceAppointmentsList;
