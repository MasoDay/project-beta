# CarCar

## Team:

* Ben DeMaso - Sales
* Esther Lee- Service

## Design

This application was designed as three microservices, using PostgreSQL for the database, Django and Python for the backend, and React and JavaScript for the frontend. Pollers assist in referencing information between microservices as needed. Other tools include full Docker integration and Bootstrap assisted with styling.

## Service microservice

I created a Technician model, a ServiceAppointment model with the technician as a foreign key to appointments, and an Automobile value object to pull data from the Inventory microservice.

Created GET, POST, PUT, DELETE calls in API for technicians and appointments to be able to view lists of technicians and apppointments, and to be able to update and/or delete them. I used the AutomobileVO in the polling function.

Created React pages for inventory lists and forms, and apointment lis

## Sales microservice

The sales microservices references the inventory microservice to find automobiles and present and record sales through several foreign keys. Users can also list and add salespeople and list and add customers.

On the backend, this is accomplished through views, models, and encoders allowing full CRUD. For example the list_or_create_sale function in views.py dictate how to fill in the Sale model using the SaleListEncoder.
